resource "osc_instance" "bastion_infra" {
  ami                         = "${var.centos7}"
  instance_type               = "${var.instance_general}"
  key_name                    = "${var.sshkey}"
  availability_zone           = "${var.region}a"
  vpc_security_group_ids      = ["${osc_security_group.ssh_bastion.id}"]
  subnet_id                   = "${osc_subnet.public_subnet.id}"
  associate_public_ip_address = true

  tags {
    Name = "bastion_infra"
  }
}

resource "osc_instance" "salt-master" {
  ami                    = "${var.centos7}"
  instance_type          = "${var.instance_general}"
  key_name               = "${var.sshkey}"
  availability_zone      = "${var.region}a"
  vpc_security_group_ids = ["${osc_security_group.private_subnet_sg.id}"]
  subnet_id              = "${osc_subnet.private_subnet.id}"

  tags {
    Name = "salt-master"
  }
}

resource "osc_instance" "nginx" {
  ami                    = "${var.centos7}"
  instance_type          = "${var.instance_general}"
  key_name               = "${var.sshkey}"
  availability_zone      = "${var.region}a"
  vpc_security_group_ids = ["${osc_security_group.web_access.id}"]
  subnet_id              = "${osc_subnet.private_subnet.id}"

  tags {
    Name = "web_nginx"
  }
}

resource "osc_instance" "jenkins" {
  ami                    = "${var.centos7}"
  instance_type          = "${var.instance_general}"
  key_name               = "${var.sshkey}"
  availability_zone      = "${var.region}a"
  vpc_security_group_ids = ["${osc_security_group.private_subnet_sg.id}"]
  subnet_id              = "${osc_subnet.private_subnet.id}"

  tags {
    Name = "jenkins"
  }
}

resource "osc_instance" "gitlab" {
  ami                    = "${var.centos7}"
  instance_type          = "${var.instance_comp_cxry}"
  key_name               = "${var.sshkey}"
  availability_zone      = "${var.region}a"
  vpc_security_group_ids = ["${osc_security_group.private_subnet_sg.id}"]
  subnet_id              = "${osc_subnet.private_subnet.id}"

  tags {
    Name = "gitlab"
  }
}
