resource "osc_route_table" "rt_public" {
    vpc_id = "${osc_vpc.vpc_ppd.id}"

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = "${osc_internet_gateway.igw.id}"
    }
}


resource "osc_route_table" "rt_private" {
    vpc_id = "${osc_vpc.vpc_ppd.id}"

    route {
        cidr_block = "0.0.0.0/0"
        nat_gateway_id = "${osc_nat_gateway.nat_gw.id}"
    }
}