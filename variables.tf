variable "centos7" {}
variable "sshkey" {}

variable "profile" {
  default = "ben"
}

variable "region" {
  default = "eu-west-2"
}

variable "availability_zone" {
  default = "eu-west-2a"
}

variable "access_bastion" {
  type = "list"
}

variable "instance_general" {}

variable "instance_comp" {}

variable "instance_mem" {}

variable "instance_comp_cxry" {}

variable "ec2" {}

variable "bastion_priv_ip" {}

variable "cidr_subnet_pub" {}

variable "cidr_subnet_priv" {}


