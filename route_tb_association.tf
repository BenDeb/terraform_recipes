resource "osc_route_table_association" "rt_asso_public" {
    subnet_id = "${osc_subnet.public_subnet.id}"
    route_table_id = "${osc_route_table.rt_public.id}"
}
resource "osc_route_table_association" "rt_asso_private" {
    subnet_id = "${osc_subnet.private_subnet.id}"
    route_table_id = "${osc_route_table.rt_private.id}"
}