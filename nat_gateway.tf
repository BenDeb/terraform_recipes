resource "osc_nat_gateway" "nat_gw"
{
    allocation_id = "${osc_eip.eip_nat_gw.id}"
    subnet_id     = "${osc_subnet.public_subnet.id}"
    depends_on    = ["osc_route_table_association.rt_asso_public"]
}