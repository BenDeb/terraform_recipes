resource "osc_subnet" "public_subnet" {
  vpc_id                  = "${osc_vpc.vpc_ppd.id}"
  cidr_block              = "${var.cidr_subnet_pub}"
  availability_zone       = "${var.region}a"
  map_public_ip_on_launch = true

  tags {
    Name = "public_subnet"
  }
}

resource "osc_subnet" "private_subnet" {
  vpc_id                 = "${osc_vpc.vpc_ppd.id}"
  cidr_block             = "${var.cidr_subnet_priv}"
  availability_zone      = "${var.region}a"

  tags {
    Name = "private_subnet"
  }
}
