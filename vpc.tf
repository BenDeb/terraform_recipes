resource "osc_vpc" "vpc_ppd" {
  cidr_block = "172.16.0.0/16"

  tags {
    Name = "vpc_ppd"
  }
}
