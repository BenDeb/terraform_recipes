resource "osc_eip" "eip_bastion" {
    vpc = true
}

resource "osc_eip" "eip_nat_gw" {
    vpc = true
}
