resource "osc_security_group" "ssh_bastion" {
  name        = "ssh_to_bastion"
  description = "SSH connections to bastion"
  vpc_id      = "${osc_vpc.vpc_ppd.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${var.access_bastion}"]
  }
}

resource "osc_security_group" "web_access" {
  name        = "Web traffic"
  description = "Web traffic"
  vpc_id      = "${osc_vpc.vpc_ppd.id}"


  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "osc_security_group" "private_subnet_sg" {
  name = "Private subnet"
  description = "Private subnet security group"
  vpc_id      = "${osc_vpc.vpc_ppd.id}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${osc_instance.bastion_infra.private_ip}"]
  }
}


 
