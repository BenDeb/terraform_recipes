centos7 = "ami-880caa66"

sshkey = "ssh-test"

access_bastion = ["46.231.147.8", "91.161.185.219"]

profile = "ben"

ec2 = "fcu.eu-west-2.outscale.com"

project = "VPC_PPD"

instance_general = "t2.medium"

instance_comp = "c4.large"

instance_mem = "m4.large"

instance_comp_cxry = "tinav4.c4r8"

region = "eu-west-2"

bastion_priv_ip = "172.16.1.31"

cidr_subnet_pub = "172.16.1.0/24"

cidr_subnet_priv = "172.16.2.0/24"
