provider "osc" {
  profile                = "${var.profile}"
  skip_region_validation = true
  region                 = "${var.region}"

  endpoints {
    ec2 = "${var.ec2}"
  }
}
